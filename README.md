![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Arkanoid-logo.svg/1280px-Arkanoid-logo.svg.png)

## Description
Simple example of Arkanoid built from using directx libraries and using cpp.

## Project
GameObject is base game object and all objects in scene derived to GameObject.
The main attributes of objects are: transform(location, rotation, size), velocity, color and colliders.
Any objects have the update and draw method for specific behaviours.
The main collision's methods are: 
 - Start: is catch when collision start;
 - Stay: is continuos catch when start collision;
 - Exit: is catch when collision end;
The objects in scene are:
 - Ball: is main object in scene and the ball bounces off all surfaces and when it hits a brick it can destroy it;
 - Brick: the are more types of brick: white, orange, green, blue, fucsia, yellow and each of them has a different score and number of hits to break;
 - Wall: they represent the containers of the scene. There are three visible walls and one invisible (kill-zone);
 - Paddle: the player controls the paddle with specific input;

## UI
The are two text in HUD. One rappresent the current score of player, and second the player's life.
When the game end, in game to appears the text gameover or congratulations.

## Input
The player might use the keyboard.
Axis movement: A | left arrow for left movement and D | right arrow for right movement.

## Technologies
Visual Studio 2022

## Plugins
directxtk_desktop_2019