#include "pch.h"
#include "Ball.h"
#include "Collider.h"
#include "GameManager.h"

Ball::Ball(FTransform nTransform, float nSpeed) : GameObject(nTransform, DirectX::Colors::Green), _speed(nSpeed)
{
	_colliders.push_back(new Collider(this, ELayer::ball, Vector2::Zero, _transform.size, DirectX::Colors::White));

	// calculate randomize rebound angle
	std::random_device dev;
	std::mt19937 rng(dev());
	std::uniform_int_distribution<std::mt19937::result_type> dist(0, 90);
	float randomAngle = (float)(dist(rng) - ANGLE_REBOUND) * DirectX::XM_PI / 180.f;

	// apply new velocity
	_velocity = Vector2::Transform(-Vector2::UnitY, DirectX::SimpleMath::Matrix::CreateRotationZ(randomAngle)) * _speed;
}

void Ball::update(float deltaTime, const Keyboard& keyboard)
{
	GameObject::update(deltaTime, keyboard);

	_switchingX = false;
	_switchingY = false;
}

void Ball::draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::draw(batch);
}

void Ball::OnCollisionStart(Collider* lhs, Collider* rhs)
{
	if(rhs->getLayer() == ELayer::killZone) {
		_velocity = Vector2::Zero;
		for (Collider* collider : _colliders) {
			if (collider != nullptr) delete collider;
		}
		_colliders.clear();
		_color = DirectX::Colors::Transparent;

		// game manager behavior
		GameManager::instance()->decrementBall();
	}

	// collision check with multi layer -> use mask
	std::vector<ELayer> tags = { ELayer::brick, ELayer::paddle, ELayer::wall };
	FLayerMask _whatIsCollidable = FLayerMask(tags);
	//Collider::collide(_whatIsCollidable, rhs->getLayer())
	if(rhs->getLayer() == ELayer::brick || rhs->getLayer() == ELayer::paddle || rhs->getLayer() == ELayer::wall)
	{
		bool revertX = false;
		bool revertY = false;
		DirectX::SimpleMath::Rectangle intersection = DirectX::SimpleMath::Rectangle::Intersect(rhs->getRectangle(), lhs->getRectangle());
		if (intersection.width >= intersection.height && !_switchingY) {
			revertY = true;
			_switchingY = true;
		}
		else if (!_switchingX) {
			revertX = true;
			_switchingX = true;
		}

		// switch axis velocity
		if (revertX) _velocity = Vector2(-_velocity.x, _velocity.y);
		if (revertY) _velocity = Vector2(_velocity.x, -_velocity.y);

		if (rhs->getLayer() == ELayer::paddle) {
			float devAngle = (_transform.position.x - rhs->getOwner()->getTransform().position.x) / lhs->getOwner()->getTransform().size.x / 2;
			devAngle *= ANGLE_REBOUND * DirectX::XM_PI / 180.0f;
			_velocity = Vector2::Transform(-Vector2::UnitY, DirectX::SimpleMath::Matrix::CreateRotationZ(devAngle)) * _speed;
		}
	}
}

void Ball::OnCollisionStay(Collider* lhs, Collider* rhs) {}

void Ball::OnCollisionExit(Collider* lhs, Collider* rhs) {}