#include "pch.h"
#include "Engine.h"

#include "GameObject.h"
#include "GameManager.h"
#include "Collider.h"

extern void ExitGame() noexcept;

using namespace DirectX;
using namespace DirectX::SimpleMath;

using Microsoft::WRL::ComPtr;

Engine::Engine() noexcept(false)
{
	_deviceResources = std::make_unique<DeviceResources>();
	_deviceResources->RegisterDeviceNotify(this);
}

void Engine ::Initialize(HWND window, int width, int height)
{
	_deviceResources->SetWindow(window, width, height);

	_deviceResources->CreateDeviceResources();
	CreateDeviceDependentResources();

	_deviceResources->CreateWindowSizeDependentResources();
	CreateWindowSizeDependentResources();

	_keyboard = std::make_unique<Keyboard>();
	_mouse = std::make_unique<Mouse>();
	_mouse->SetWindow(window);

	GameManager::instance()->setCenter(_center);
	GameManager::instance()->initGame();
}

void Engine::Tick()
{
	_timer.Tick([&]()
	{
		Update(_timer);
	});

	Render();
}

void Engine::Update(StepTimer const& timer)
{
	float elapsedTime = float(timer.GetElapsedSeconds());

	GameManager::instance()->updateTime(timer.GetTotalSeconds());

	// powerup check 
	GameManager::instance()->checkPowerupActive();

	auto kb = _keyboard->GetState();
	_keys.Update(kb);

	if (kb.Escape)
	{
		ExitGame();
	}
	if (kb.R) {
		GameManager::instance()->initGame();
	}
	if (GameManager::instance()->getBricksCount() > 0 && GameManager::instance()->getLifesCount() > 0) {
		std::shared_ptr<GameObject> pawn = GameManager::instance()->getPawn();
		if (pawn != nullptr)	pawn->update(elapsedTime, kb);

		for (auto obj : GameManager::instance()->getBalls()) {
			if (obj != nullptr) obj->update(elapsedTime, kb);
		}

		for (auto obj : GameManager::instance()->getStaticObjectsInScene()) {
			if(obj != nullptr) obj->update(elapsedTime, kb);
		}

		for (auto obj : GameManager::instance()->getDynamicObjectsInScene()) {
			if (obj != nullptr) obj->update(elapsedTime, kb);
		}
	}
}

void Engine::Render()
{
	if (_timer.GetFrameCount() == 0) return;

	Clear();

	_deviceResources->PIXBeginEvent(L"Render");
	auto context = _deviceResources->GetD3DDeviceContext();
	context->OMSetBlendState(_states->Opaque(), nullptr, 0xFFFFFFFF);
	context->OMSetDepthStencilState(_states->DepthNone(), 0);
	context->RSSetState(_states->CullNone());

	_effect->Apply(context);

	context->IASetInputLayout(_inputLayout.Get());

	// draw all abjects in scene
	_batch->Begin();

	for (auto obj : GameManager::instance()->getStaticObjectsInScene()) {
		if (obj != nullptr) obj->draw(_batch.get());
	}

	for (auto obj : GameManager::instance()->getDynamicObjectsInScene()) {
		if (obj != nullptr) obj->draw(_batch.get());
	}

	std::shared_ptr<GameObject> pawn = GameManager::instance()->getPawn();
	if (pawn != nullptr)	pawn->draw(_batch.get());

	for (auto obj : GameManager::instance()->getBalls()) {
		if (obj != nullptr) obj->draw(_batch.get());
	}

	_batch->End();

	// draw all text in scene
	_spriteBatch->Begin();

	std::string str;
	std::wstring widestr;
	Vector2 origin = Vector2::Zero;

	if (GameManager::instance()->getLifesCount() > 0 || GameManager::instance()->getBricksCount() > 0)
	{
		// draw life counter
		str = "Life: " + std::to_string(GameManager::instance()->getLifesCount());
		widestr = std::wstring(str.begin(), str.end());
		const wchar_t* life = widestr.c_str();
		origin = _font->MeasureString(life) / 2.f;
		_font->DrawString(_spriteBatch.get(), life, Vector2(60.f, 40.f), Colors::White, 0, origin);

		// draw life counter
		str = "Score: " + std::to_string(GameManager::instance()->getScore());
		widestr = std::wstring(str.begin(), str.end());
		const wchar_t* score = widestr.c_str();
		origin = _font->MeasureString(score) / 2.f;
		_font->DrawString(_spriteBatch.get(), score, Vector2(160.f, 40.f), Colors::White, 0, origin);
	}

	if (GameManager::instance()->getLifesCount() == 0)
	{
		// draw game over
		const wchar_t* gameover = L"GAME OVER!!!";
		origin = _font->MeasureString(gameover) / 2.f;
		_font->DrawString(_spriteBatch.get(), gameover, _fontPos, Colors::Red, 0, origin, Vector2::One * 2.f);
	}
	else if (GameManager::instance()->getBricksCount() == 0)
	{
		// draw game over
		const wchar_t* gameover = L"Congratulations!!!";
		origin = _font->MeasureString(gameover) / 2.f;
		_font->DrawString(_spriteBatch.get(), gameover, _fontPos, Colors::Blue, 0, origin, Vector2::One * 2.f);
	}

	_spriteBatch->End();

	_deviceResources->PIXEndEvent();
	_deviceResources->Present();

	_graphicsMemory->Commit();
}

void Engine::Clear()
{
	_deviceResources->PIXBeginEvent(L"Clear");

	auto context = _deviceResources->GetD3DDeviceContext();
	auto renderTarget = _deviceResources->GetRenderTargetView();
	auto depthStencil = _deviceResources->GetDepthStencilView();

	context->ClearRenderTargetView(renderTarget, Colors::Black);
	context->ClearDepthStencilView(depthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	context->OMSetRenderTargets(1, &renderTarget, depthStencil);

	auto viewport = _deviceResources->GetScreenViewport();
	context->RSSetViewports(1, &viewport);

	_deviceResources->PIXEndEvent();
}

void Engine::OnActivated()
{
	_keys.Reset();
	_mouseButtons.Reset();
}

void Engine::OnDeactivated() {}

void Engine::OnSuspending() {}

void Engine::OnResuming()
{
	_timer.ResetElapsedTime();
	_keys.Reset();
	_mouseButtons.Reset();
}

void Engine::OnWindowMoved()
{
	auto r = _deviceResources->GetOutputSize();
	_deviceResources->WindowSizeChanged(r.right, r.bottom);
}

void Engine::OnWindowSizeChanged(int width, int height)
{
	if (!_deviceResources->WindowSizeChanged(width, height))
		return;

	CreateWindowSizeDependentResources();

}

void Engine::GetDefaultSize(int& width, int& height) const noexcept
{
	width = 800;
	height = 600;
}

void Engine::CreateDeviceDependentResources()
{
	auto device = _deviceResources->GetD3DDevice();
	_states = std::make_unique<CommonStates>(device);
	_graphicsMemory = std::make_unique<GraphicsMemory>(device);
	
	auto context = _deviceResources->GetD3DDeviceContext();
	_spriteBatch = std::make_unique<SpriteBatch>(context);
	_effect = std::make_unique<BasicEffect>(device);
	_batch = std::make_unique<PrimitiveBatch<DirectX::VertexPositionColor>>(context);
	_effect->SetVertexColorEnabled(true);

	_font = std::make_unique<SpriteFont>(device, L"myfile.spritefont");

	ThrowIfFailed( CreateInputLayoutFromEffect<DirectX::VertexPositionColor>(device, _effect.get(), _inputLayout.ReleaseAndGetAddressOf()));
}

void Engine::CreateWindowSizeDependentResources()
{
	auto size = _deviceResources->GetOutputSize();
	_center.x = float(size.right) / 2.0f;
	_center.y = float(size.bottom) / 2.0f;
	_fontPos.x = _center.x;
	_fontPos.y = _center.y;
	Matrix proj = Matrix::CreateScale(2.f / float(size.right), -2.f / float(size.bottom), 1.f) * Matrix::CreateTranslation(-1.f, 1.f, 0.f);

	_effect->SetProjection(proj);
}

void Engine::OnDeviceLost()
{
	_graphicsMemory.reset();
	_states.reset();
	_effect.reset();
	_batch.reset();
	_font.reset();
	_spriteBatch.reset();
	_inputLayout.Reset();
}

void Engine::OnDeviceRestored()
{
	CreateDeviceDependentResources();

	CreateWindowSizeDependentResources();
}