#pragma once
#include "GameObject.h"

class Ball : public GameObject
{
private:
	static constexpr float ANGLE_REBOUND = 35.f;

	float _speed;
	bool _switchingX = false;
	bool _switchingY = false;

public:
	Ball(FTransform nTransform, float nSpeed = 300.f);

	// main behavior
	virtual void update(float deltaTime, const Keyboard& keyboard) override;
	virtual void draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;

	// collision check
	virtual void OnCollisionStart(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionStay(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionExit(Collider* lhs, Collider* rhs) override;

};