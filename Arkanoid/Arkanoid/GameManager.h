#pragma once
#include <map>
#include <tuple>
#include "Collider.h"

class GameObject;

class GameManager
{
private:
	using Vector2 = DirectX::SimpleMath::Vector2;
	using Quaternion = DirectX::SimpleMath::Quaternion;

	static constexpr Vector2 PADDLE_POSITION_DEFAULT = Vector2(0, 200.f);
	static constexpr Vector2 BALL_POSITION_DEFAULT = Vector2(0, 180.f);

	static constexpr Vector2 PADDLE_SIZE_DEFAULT = Vector2(100.0f, 25.0f);
	static constexpr Vector2 BALL_SIZE_DEFAULT = Vector2(15.0f, 15.0f);
	static constexpr Vector2 WALL_SIZE_DEFAULT = Vector2(780.f, 580.f);
	static constexpr Vector2 BRICK_SIZE_DEFAULT = Vector2(80.0f, 30.0f);
	static constexpr Vector2 POWERUP_SIZE_DEFAULT = Vector2(30.0f, 20.0f);

	static constexpr float POWERUP_ACTIVATION_MAX_TIME = 4.5f;
	static constexpr float POWERUP_INCREMENTE_SIZE = 1.5f;
	static constexpr float POWERUP_SPAWN_FACTOR = 1.f;
	static constexpr unsigned short POWERUP_SPAWN_BALLS_LIMIT = 3;

	static constexpr unsigned short LIFE_MAX_COUNT = 3;

private:
	unsigned short _bricksCount = 0;
	unsigned short _lifesCount = 0;
	unsigned short _rows = 3;
	unsigned short _cols = 8;
	unsigned int _score = 0;

	float _powerupIncrementFactor = 0;

	float _currentTime = 0;
	float _startPowerupTime = 0;

	Vector2 _center = Vector2::Zero;
	std::vector<std::shared_ptr<GameObject>> _staticObjectsInScene;
	std::vector<std::shared_ptr<GameObject>> _dynamicObjectsInScene;
	std::vector<std::shared_ptr<GameObject>> _balls;
	std::shared_ptr<GameObject> _pawn;

	// powerup check
	bool _hasPowerupPaddle = false;
	unsigned short _ballsCount = 0;

private:
	std::map<ELayer, FLayerMask> _collisionsMask = {
		{ELayer::none, FLayerMask()},
		{ELayer::wall, FLayerMask(std::vector<ELayer> { ELayer::paddle, ELayer::ball})},
		{ELayer::paddle, FLayerMask(std::vector<ELayer> { ELayer::wall, ELayer::ball, ELayer::powerup})},
		{ELayer::brick, FLayerMask(std::vector<ELayer> { ELayer::ball})},
		{ELayer::ball, FLayerMask(std::vector<ELayer> { ELayer::paddle, ELayer::wall, ELayer::brick, ELayer::killZone})},
		{ELayer::killZone, FLayerMask(std::vector<ELayer> { ELayer::ball})},
		{ELayer::powerup, FLayerMask(std::vector<ELayer> { ELayer::paddle, ELayer::killZone})},
	};

	std::map<unsigned short, std::tuple<DirectX::XMVECTORF32, unsigned short, unsigned short>> _brickTypeMedium = {
		{ 0, { DirectX::Colors::White, 50, 1}},
		{ 1, { DirectX::Colors::Orange, 60, 1}},
		{ 2, { DirectX::Colors::Green, 80, 2}},
		{ 3, { DirectX::Colors::Blue, 90, 2}},
		{ 4, { DirectX::Colors::Fuchsia, 110, 3}},
		{ 5, { DirectX::Colors::Yellow, 120, 3}}
	};

	std::map<unsigned short, std::tuple<DirectX::XMVECTORF32, unsigned short, unsigned short>> _brickTypeEasy = {
		{ 2, { DirectX::Colors::White, 50, 1}},
		{ 1, { DirectX::Colors::Orange, 60, 1}},
		{ 0, { DirectX::Colors::Green, 80, 2}}
	};

private:
	GameManager();

public:
	GameManager(const GameManager&) = delete;
	GameManager& operator=(const GameManager&) = delete;

	// instance
	static std::shared_ptr<GameManager> instance()
	{
		static std::shared_ptr<GameManager> s{ new GameManager };
		return s;
	}

	// main behavior
	std::vector<Collider*> getCollidersInSceneWithLayer(ELayer tag) const;
	void initGame();
	void generateBricksEasyGame();
	void generateBricksMediumGame();

	// ball behavior
	void decrementBall();

	// powerup
	void generatePowerup(Vector2 spawnPosition);
	void checkPowerupActive();
	void powerupChangeSize();
	void powerupSpawnBalls();

	// get and set
	inline unsigned short getBricksCount() const { return _bricksCount; }
	inline unsigned short getLifesCount() const { return _lifesCount; }
	inline Vector2 getCenter() const { return _center; }
	inline unsigned int getScore() const { return _score; }
	inline void decrementBricks() { _bricksCount--; }
	inline void incrementScore(unsigned short value = 1) { _score += value; }
	inline void setCenter(Vector2 nCenter) { _center = nCenter; }
	inline void updateTime(float time) { _currentTime = time; }

	inline std::shared_ptr<GameObject> getPawn() const { return _pawn; }
	inline std::vector<std::shared_ptr<GameObject>> getBalls() const { return _balls; }
	inline std::vector<std::shared_ptr<GameObject>> getStaticObjectsInScene() const { return _staticObjectsInScene; }
	inline std::vector<std::shared_ptr<GameObject>> getDynamicObjectsInScene() const { return _dynamicObjectsInScene; }

private:
	void cleanObjects();
	void spawnBall(Vector2 position = Vector2::Zero);

	void decrementLifes();
	void gameover();

};