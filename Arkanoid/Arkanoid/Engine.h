#pragma once
#include "DeviceResources.h"
#include "StepTimer.h"

class GameManager;

class Engine : public IDeviceNotify
{
private:
	std::unique_ptr<DeviceResources> _deviceResources;
	StepTimer _timer;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> _texture;
	std::unique_ptr<DirectX::GraphicsMemory> _graphicsMemory;
	std::unique_ptr<DirectX::SpriteBatch> _spriteBatch;
	std::unique_ptr<DirectX::CommonStates> _states;
	std::unique_ptr<DirectX::BasicEffect> _effect;
	std::unique_ptr<DirectX::Keyboard> _keyboard;
	DirectX::Keyboard::KeyboardStateTracker _keys;
	DirectX::Mouse::ButtonStateTracker _mouseButtons;
	std::unique_ptr<DirectX::Mouse> _mouse;
	std::unique_ptr<DirectX::PrimitiveBatch<DirectX::VertexPositionColor>> _batch;
	Microsoft::WRL::ComPtr<ID3D11InputLayout> _inputLayout;
	DirectX::SimpleMath::Vector2 _center;
	std::unique_ptr<DirectX::SpriteFont> _font;
	DirectX::SimpleMath::Vector2 _fontPos;

public:
	Engine() noexcept(false);
	~Engine() = default;
	Engine(Engine&&) = default;
	Engine& operator= (Engine&&) = default;
	Engine(Engine const&) = delete;
	Engine& operator= (Engine const&) = delete;

	// main behavior
	void Initialize(HWND window, int width, int height);
	void Tick();

	void OnDeviceLost() override;
	void OnDeviceRestored() override;
	void OnActivated();
	void OnDeactivated();
	void OnSuspending();
	void OnResuming();
	void OnWindowMoved();
	void OnWindowSizeChanged(int width, int height);
	void GetDefaultSize(int& width, int& height) const noexcept;

private:
	void Update(StepTimer const& timer);
	void Render();
	void Clear();

	void CreateDeviceDependentResources();
	void CreateWindowSizeDependentResources();

};