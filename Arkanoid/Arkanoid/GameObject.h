#pragma once
#include "Collider.h"

struct FTransform
{
public:
	using Vector2 = DirectX::SimpleMath::Vector2;
	using Quaternion = DirectX::SimpleMath::Quaternion;

public:
	Vector2 position;
	Quaternion rotation;
	Vector2 size;

public:
	FTransform(Vector2 nPosition = Vector2::Zero, Quaternion nRotation = Quaternion::Identity, Vector2 nSize = Vector2::One) : position(nPosition), rotation(nRotation), size(nSize) {}
};

class GameObject
{
protected:
	using Vector2 = DirectX::SimpleMath::Vector2;
	using Keyboard = DirectX::Keyboard::State;

	static constexpr float VELOCITY_MIN = .001f;

protected:
	// main attributes
	FTransform _transform;
	Vector2 _velocity;

	// color attributes
	DirectX::XMVECTORF32 _color;

	// collision attributes
	std::vector<Collider*> _colliders;

public:
	// constructors
	GameObject(FTransform nTransform = FTransform(), DirectX::XMVECTORF32 nColor = DirectX::Colors::Transparent);
	~GameObject();

	// main behavior
	virtual void update(float deltaTime, const Keyboard& keyboard);
	virtual void draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const;

	// collision check
	virtual void OnCollisionStart(Collider* lhs, Collider* rhs) = 0;
	virtual void OnCollisionStay(Collider* lhs, Collider* rhs) = 0;
	virtual void OnCollisionExit(Collider* lhs, Collider* rhs) = 0;

	// get and set
	inline FTransform getTransform() const { return _transform; }
	inline std::vector<Collider*> getColliders() const { return _colliders; }
	std::vector<Collider*> getCollidersWithLayerMask(FLayerMask layermask) const;

};