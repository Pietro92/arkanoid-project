#include "pch.h"
#include "Brick.h"
#include "Collider.h"
#include "GameManager.h"

Brick::Brick(FTransform nTransform, DirectX::XMVECTORF32 nBrickColor, unsigned short nScore, unsigned short nHitCount) : GameObject(nTransform, DirectX::Colors::Red), _score(nScore), _hitCount(nHitCount)
{
	_colliders.push_back(new Collider(this, ELayer::brick, Vector2::Zero, _transform.size, nBrickColor));
}

void Brick::update(float deltaTime, const Keyboard& keyboard)
{
	GameObject::update(deltaTime, keyboard);
}

void Brick::draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::draw(batch);
}

void Brick::OnCollisionStart(Collider* lhs, Collider* rhs)
{
	if (rhs->getLayer() == ELayer::ball)
	{
		if (_hitCount > 0)
		{
			_hitCount--;
			return;
		}

		for (auto col : _colliders)
		{
			if (col != nullptr) delete col;
		}
		_colliders.clear();

		// update color
		_color = DirectX::Colors::Transparent;

		// update game manager
		GameManager::instance()->decrementBricks();
		GameManager::instance()->incrementScore(_score);
		GameManager::instance()->generatePowerup(_transform.position);
	}
}

void Brick::OnCollisionStay(Collider* lhs, Collider* rhs) {}

void Brick::OnCollisionExit(Collider* lhs, Collider* rhs) {}