#include "pch.h"
#include "Wall.h"
#include "Collider.h"

Wall::Wall(FTransform nTransform) : GameObject(nTransform, DirectX::Colors::Transparent)
{
	float colliderHalfSize = 25.f;
	float extraOffset = 10.f;

	_colliders.push_back(new Collider(this, ELayer::wall, Vector2(-(_transform.size.x / 2) - colliderHalfSize, 0.0f), Vector2(colliderHalfSize, (_transform.size.y / 2) + extraOffset) * 2.0f, DirectX::Colors::CornflowerBlue));
	_colliders.push_back(new Collider(this, ELayer::wall, Vector2(0.0f, -(_transform.size.y / 2) - colliderHalfSize), Vector2((_transform.size.x / 2) + extraOffset, colliderHalfSize) * 2.0f, DirectX::Colors::CornflowerBlue));
	_colliders.push_back(new Collider(this, ELayer::wall, Vector2((_transform.size.x / 2) + colliderHalfSize, 0.0f), Vector2(colliderHalfSize, (_transform.size.y / 2) + extraOffset) * 2.0f, DirectX::Colors::CornflowerBlue));
	_colliders.push_back(new Collider(this, ELayer::killZone, Vector2(0.0f, (_transform.size.y / 2) + colliderHalfSize), Vector2((_transform.size.x / 2) + extraOffset, colliderHalfSize) * 2.0f, DirectX::Colors::CornflowerBlue));
}

void Wall::update(float deltaTime, const Keyboard& keyboard)
{
	GameObject::update(deltaTime, keyboard);
}

void Wall::draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::draw(batch);
}

void Wall::OnCollisionStart(Collider* lhs, Collider* rhs) {}

void Wall::OnCollisionStay(Collider* lhs, Collider* rhs) {}

void Wall::OnCollisionExit(Collider* lhs, Collider* rhs) {}