#include "pch.h"
#include "PowerupIncrementSize.h"
#include "Collider.h"
#include "GameManager.h"

PowerupIncrementSize::PowerupIncrementSize(FTransform nTransform) : Powerup(nTransform, DirectX::Colors::Orange) {}

void PowerupIncrementSize::ActivePowerup()
{
	GameManager::instance()->powerupChangeSize();
}