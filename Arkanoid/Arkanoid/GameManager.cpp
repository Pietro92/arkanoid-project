#include "pch.h"
#include "GameManager.h"

#include "GameObject.h"
#include "Wall.h"
#include "Paddle.h"
#include "Brick.h"
#include "Ball.h"
#include "Powerup.h"
#include "PowerupMultiball.h"
#include "PowerupIncrementSize.h"

GameManager::GameManager()
{
	_staticObjectsInScene = std::vector<std::shared_ptr<GameObject>>();
	_dynamicObjectsInScene = std::vector<std::shared_ptr<GameObject>>();
	_balls = std::vector<std::shared_ptr<GameObject>>();
}

std::vector<Collider*> GameManager::getCollidersInSceneWithLayer(ELayer tag) const
{
	std::vector<Collider*> colliders = std::vector<Collider*>();
	std::vector<Collider*> tmp = std::vector<Collider*>();
	FLayerMask layermask = _collisionsMask.find(tag)->second;

	tmp = _pawn->getCollidersWithLayerMask(layermask);
	colliders.insert(colliders.end(), tmp.begin(), tmp.end());
	tmp.clear();

	for (auto obj : _balls)
	{
		if (obj != nullptr)
		{
			tmp = obj->getCollidersWithLayerMask(layermask);
			colliders.insert(colliders.end(), tmp.begin(), tmp.end());
		}
	}
	tmp.clear();

	for (auto obj : _staticObjectsInScene)
	{
		if (obj != nullptr)
		{
			tmp = obj->getCollidersWithLayerMask(layermask);
			colliders.insert(colliders.end(), tmp.begin(), tmp.end());
		}
	}
	tmp.clear();

	for (auto obj : _dynamicObjectsInScene)
	{
		if (obj != nullptr)
		{
			tmp = obj->getCollidersWithLayerMask(layermask);
			colliders.insert(colliders.end(), tmp.begin(), tmp.end());
		}
	}
	tmp.clear();

	return colliders;
}

void GameManager::decrementBall()
{
	_ballsCount--;
	if (_ballsCount == 0) decrementLifes();
}

void GameManager::decrementLifes()
{
	_lifesCount--;

	if (_lifesCount > 0) 
	{
	    spawnBall();
		return;
	}

	gameover();
}

void GameManager::gameover()
{
	cleanObjects();

	_bricksCount = 0;
	_lifesCount = 0;
}

void GameManager::initGame()
{
	cleanObjects();

	// init setup params
	_lifesCount = LIFE_MAX_COUNT;
	_bricksCount = _rows * _cols;

	// add ball
	spawnBall();

	// add pawn (paddle)
	FTransform paddleTransform = FTransform(_center + PADDLE_POSITION_DEFAULT, Quaternion::Identity, PADDLE_SIZE_DEFAULT);
	_pawn = std::shared_ptr<Paddle>(new Paddle(paddleTransform));
	
	// add walls
	FTransform wallTransform = FTransform(_center, Quaternion::Identity, WALL_SIZE_DEFAULT);
	_staticObjectsInScene.push_back(std::shared_ptr<Wall>(new Wall(wallTransform)));
	
	// select randomize difficulty
	float percentage = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	if (percentage < .7f)
	{
		generateBricksEasyGame();
	}
	else
	{
		generateBricksMediumGame();
	}
}

void GameManager::generateBricksEasyGame()
{
	// add bricks
	// generate randomize brick 
	Vector2 centerOfBrick = _center - PADDLE_POSITION_DEFAULT;
	srand(time(NULL));
	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _cols; j++) {
			// generate position 
			Vector2 currentBrickPosition = centerOfBrick + Vector2(-280.0f + j * 80.0f, 0.0f + i * 30.0f + 20.0f * i);
			FTransform brickTransform = FTransform(currentBrickPosition, Quaternion::Identity, BRICK_SIZE_DEFAULT);

			// init brick 
			auto currentBrick = _brickTypeEasy.find(i);
			std::tuple<DirectX::XMVECTORF32, float, float> currentBrickAttributes = currentBrick->second;
			DirectX::XMVECTORF32 currentBrickColor = std::get<DirectX::XMVECTORF32>(currentBrickAttributes);
			_staticObjectsInScene.push_back(std::shared_ptr<Brick>(new Brick(brickTransform, currentBrickColor, std::get<1>(currentBrickAttributes), std::get<2>(currentBrickAttributes))));
		}
	}
}

void GameManager::generateBricksMediumGame()
{
	// add bricks
	// the first row is composite with green brick -> one point and one hit for destroy
	// the second row is composite with pink brick -> two points and two hit for destroy
	// the third row is composite with yellow brick -> three points and three hit for destroy
	Vector2 centerOfBrick = _center - PADDLE_POSITION_DEFAULT;
	srand(time(NULL));
	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _cols; j++) {
			// generate position 
			Vector2 currentBrickPosition = centerOfBrick + Vector2(-280.0f + j * 80.0f, 0.0f + i * 30.0f + 20.0f * i);
			FTransform brickTransform = FTransform(currentBrickPosition, Quaternion::Identity, BRICK_SIZE_DEFAULT);

			// generate randomizer brick index
			unsigned short index = std::rand() % _brickTypeMedium.size();

			// init brick 
			auto currentBrick = _brickTypeMedium.find(index);
			std::tuple<DirectX::XMVECTORF32, float, float> currentBrickAttributes = currentBrick->second;
			DirectX::XMVECTORF32 currentBrickColor = std::get<DirectX::XMVECTORF32>(currentBrickAttributes);
			_staticObjectsInScene.push_back(std::shared_ptr<Brick>(new Brick(brickTransform, currentBrickColor, std::get<1>(currentBrickAttributes), std::get<2>(currentBrickAttributes))));
		}
	}
}

void GameManager::cleanObjects()
{
	_pawn = nullptr;
	_balls.clear();
	_staticObjectsInScene.clear();
	_dynamicObjectsInScene.clear();
}

void GameManager::spawnBall(Vector2 position)
{
	FTransform ballTransform = FTransform(_center + BALL_POSITION_DEFAULT + position, Quaternion::Identity, BALL_SIZE_DEFAULT);
	_balls.push_back(std::shared_ptr<Ball>(new Ball(ballTransform)));

	// this increment is important for powerup because when a ball collide with killzone controll if ball count is == 0
	_ballsCount++;
}

void GameManager::generatePowerup(Vector2 spawnPosition)
{
	float percentage = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	if (percentage + _powerupIncrementFactor <= POWERUP_SPAWN_FACTOR)
	{
		_powerupIncrementFactor += .1f;
		return;
	}

	// reset factor
	_powerupIncrementFactor = 0;

	// pick randomize powerup
	srand(time(0));
	unsigned short index = std::rand() % 2;	// MultipliBall is max count of enum
	FTransform powerupTransform = FTransform(spawnPosition, Quaternion::Identity, POWERUP_SIZE_DEFAULT);
	switch ((EPowerupType)index)
	{
	case EPowerupType::IncrementSize:
		_dynamicObjectsInScene.push_back(std::shared_ptr<PowerupIncrementSize>(new PowerupIncrementSize(powerupTransform)));
		break;
	case EPowerupType::MutipliBall:
		_dynamicObjectsInScene.push_back(std::shared_ptr<PowerupMultiball>(new PowerupMultiball(powerupTransform)));
		break;
	}
}

void GameManager::checkPowerupActive()
{
	if (_hasPowerupPaddle && _currentTime >= _startPowerupTime + POWERUP_ACTIVATION_MAX_TIME)
	{
		_pawn->getTransform().size = PADDLE_SIZE_DEFAULT;
		_hasPowerupPaddle = false;
		_startPowerupTime = 0;

		dynamic_cast<Paddle*>(_pawn.get())->defaultSize(PADDLE_SIZE_DEFAULT);
	}
}

void GameManager::powerupChangeSize()
{
	if (_hasPowerupPaddle)	return;

	_startPowerupTime = _currentTime;
	_hasPowerupPaddle = true;

	dynamic_cast<Paddle*>(_pawn.get())->incrementSize(POWERUP_INCREMENTE_SIZE);
}

void GameManager::powerupSpawnBalls()
{
	unsigned short limit = POWERUP_SPAWN_BALLS_LIMIT - _ballsCount;

	for (unsigned short i = 0; i < limit; i++)
	{
		float randomX = static_cast <float> (rand() % 50);
		float randomY = static_cast <float> (rand() % 20);
		Vector2 position = Vector2(randomX, -randomY);
		spawnBall(position);
	}
}