#pragma once
#include <iostream>
#include <vector>
#include <bitset>

class GameObject;

enum ELayer
{
	none,
	wall,
	paddle,
	brick,
	ball,
	killZone,
	powerup,
	mixed
};

struct FLayerMask
{
public:
	std::bitset<mixed> _mask;

public:
	FLayerMask(ELayer tag = ELayer::none) { _mask.set(tag); }
	FLayerMask(std::vector<ELayer> tags)
	{
		for (ELayer tag : tags)
		{
			_mask.set(tag);
		}
	}

	inline std::bitset<mixed> operator|(ELayer tag)
	{
		return _mask | std::bitset<mixed>(1 << tag);
	}

	inline bool operator==(std::bitset<mixed> rhs)
	{
		return _mask == rhs;
	}
};

class Collider
{
private:
	using Vector2 = DirectX::SimpleMath::Vector2;

private:
	static constexpr float PADDING_COLLIDER = 1.f;

	GameObject* _owner;
	ELayer _layer;
	Vector2 _center;
	Vector2 _size;
	DirectX::XMVECTORF32 _color;

	std::vector<Collider*> _collisions;
	std::vector<Collider*> _currentCollisions;
	std::vector<Collider*> _oldCollisions;
	std::vector<Collider*> _finishedCollisions;
	std::vector<Collider*> _tempCollisions;

public:
	// constructor
	Collider(GameObject* nOwner, ELayer nTag = ELayer::none, Vector2 nCenter = Vector2::Zero, Vector2 nSize = Vector2::Zero, DirectX::XMVECTORF32 nColor = DirectX::Colors::Blue);

	// main behavior
	void update();
	void draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const;
	DirectX::SimpleMath::Rectangle getRectangle();

	// physic attributes
	static bool canCollide(Collider* clhs, Collider* crhs);
	static bool collide(FLayerMask layermask, ELayer tag);

	// get
	inline Vector2 getCenter() const { return _center; }
	inline Vector2 getSize() const { return _size; }
	inline ELayer getLayer() const { return _layer; }
	inline GameObject* getOwner() const { return _owner; }
	inline std::vector<Collider*> getStartColliders() { return _currentCollisions; }
	inline std::vector<Collider*> getStayColliders() { return _oldCollisions; }
	inline std::vector<Collider*> getExitColliders() { return _finishedCollisions; }
	inline void setSize(Vector2 nSize) { _size = nSize; }

private:
	void updateStatusOfCollisions();

};