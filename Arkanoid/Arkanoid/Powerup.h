#pragma once
#include "GameObject.h"

enum EPowerupType
{
	IncrementSize,
	MutipliBall,
};

class Powerup : public GameObject
{
protected:
	virtual void ActivePowerup() = 0;

public:
	Powerup(FTransform nTransform, DirectX::XMVECTORF32 nPowerupColor);
	virtual void update(float deltaTime, const Keyboard& keyboard) override;
	virtual void draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;

	virtual void OnCollisionStart(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionStay(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionExit(Collider* lhs, Collider* rhs) override;
};