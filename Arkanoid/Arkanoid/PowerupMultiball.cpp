#include "pch.h"
#include "PowerupMultiball.h"
#include "Collider.h"
#include "GameManager.h"

PowerupMultiball::PowerupMultiball(FTransform nTransform) : Powerup(nTransform, DirectX::Colors::Fuchsia) {}

void PowerupMultiball::ActivePowerup()
{
	GameManager::instance()->powerupSpawnBalls();
}