#include "pch.h"
#include "Paddle.h"
#include "Collider.h"

Paddle::Paddle(FTransform nTransform, float nSpeed) : GameObject(nTransform, DirectX::Colors::Red), _speed(nSpeed) 
{
	_colliders.push_back(new Collider(this, ELayer::paddle, Vector2::Zero, _transform.size, DirectX::Colors::Gray));
}

void Paddle::update(float deltaTime, const Keyboard& keyboard)
{
	GameObject::update(deltaTime, keyboard);
	float axisMultiply = 0;

	if (keyboard.Left || keyboard.A) {
		axisMultiply = -1.f;
	}
	if (keyboard.Right || keyboard.D) {
		axisMultiply = 1.f;
	}
	_velocity = Vector2::UnitX * _speed * axisMultiply;
}

void Paddle::draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::draw(batch);
}

void Paddle::OnCollisionStart(Collider* lhs, Collider* rhs)
{
	if (rhs->getLayer() == ELayer::wall) {
		DirectX::SimpleMath::Rectangle wallRect = rhs->getRectangle();
		if (_transform.position.x > wallRect.Center().x) {
			_transform.position = Vector2(wallRect.x + wallRect.width + _transform.size.x / 2, _transform.position.y);
		}
		else {
			_transform.position = Vector2(wallRect.x - _transform.size.x / 2, _transform.position.y);
		}
	}
}

void Paddle::OnCollisionStay(Collider* lhs, Collider* rhs)
{
	if (rhs->getLayer() == ELayer::wall) {
		DirectX::SimpleMath::Rectangle wallRect = rhs->getRectangle();
		if (_transform.position.x > wallRect.Center().x) {
			_transform.position = Vector2(wallRect.x + wallRect.width + _transform.size.x / 2, _transform.position.y);
		}
		else {
			_transform.position = Vector2(wallRect.x - _transform.size.x / 2, _transform.position.y);
		}
	}
}

void Paddle::OnCollisionExit(Collider* lhs, Collider* rhs) {}

void Paddle::incrementSize(float multiplier)
{
	Vector2 nSize = Vector2(_transform.size.x * multiplier, _transform.size.y);

	// update object
	_transform.size = nSize;

	//update collisions
	for (auto col : _colliders)
	{
		col->setSize(nSize);
	}
}

void Paddle::decrementSize(float multiplier)
{
	Vector2 nSize = Vector2(_transform.size.x / multiplier, _transform.size.y);

	// update object
	_transform.size = nSize;

	//update collisions
	for (auto col : _colliders)
	{
		col->setSize(nSize);
	}
}

void Paddle::defaultSize(Vector2 size)
{
	_transform.size = size;

	//update collisions
	for (auto col : _colliders)
	{
		col->setSize(size);
	}
}