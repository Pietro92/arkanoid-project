#pragma once
#include "Powerup.h"

class PowerupMultiball : public Powerup
{
protected:
	virtual void ActivePowerup();

public:
	PowerupMultiball(FTransform nTransform);
};