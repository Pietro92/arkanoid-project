#pragma once
#include "Powerup.h"

class PowerupIncrementSize : public Powerup
{
protected:
	virtual void ActivePowerup();

public:
	PowerupIncrementSize(FTransform nTransform);
};