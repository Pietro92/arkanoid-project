#pragma once
#include "GameObject.h"

class Wall : public GameObject
{
public:
	Wall(FTransform nTransform);
	virtual void update(float deltaTime, const Keyboard& keyboard) override;
	virtual void draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;

	virtual void OnCollisionStart(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionStay(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionExit(Collider* lhs, Collider* rhs) override;
};