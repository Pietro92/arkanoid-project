#include "pch.h"

#include "Collider.h"
#include "GameObject.h"
#include "GameManager.h"

Collider::Collider(GameObject* nOwner, ELayer nTag, Vector2 nCenter, Vector2 nSize, DirectX::XMVECTORF32 nColor) : _layer(nTag), _center(nCenter), _size(nSize), _color(nColor)
{
	_owner = nOwner;

	_collisions = std::vector<Collider*>();
	_currentCollisions = std::vector<Collider*>();
	_oldCollisions = std::vector<Collider*>();
	_finishedCollisions = std::vector<Collider*>();
	_tempCollisions = std::vector<Collider*>();
}

void Collider::update()
{
	updateStatusOfCollisions();
}

void Collider::draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	DirectX::VertexPositionColor vertex1(Vector2(_owner->getTransform().position.x + _center.x - _size.x / 2 + PADDING_COLLIDER, _owner->getTransform().position.y + _center.y - _size.y / 2 + PADDING_COLLIDER), _color);
	DirectX::VertexPositionColor vertex2(Vector2(_owner->getTransform().position.x + _center.x + _size.x / 2 - PADDING_COLLIDER, _owner->getTransform().position.y + _center.y - _size.y / 2 + PADDING_COLLIDER), _color);
	DirectX::VertexPositionColor vertex3(Vector2(_owner->getTransform().position.x + _center.x + _size.x / 2 - PADDING_COLLIDER, _owner->getTransform().position.y + _center.y + _size.y / 2 - PADDING_COLLIDER), _color);
	DirectX::VertexPositionColor vertex4(Vector2(_owner->getTransform().position.x + _center.x - _size.x / 2 + PADDING_COLLIDER, _owner->getTransform().position.y + _center.y + _size.y / 2 - PADDING_COLLIDER), _color);
	batch->DrawQuad(vertex1, vertex2, vertex3, vertex4);
}

DirectX::SimpleMath::Rectangle Collider::getRectangle()
{
	float width = _owner->getTransform().position.x + _center.x - _size.x / 2;
	float height = _owner->getTransform().position.y + _center.y - _size.y / 2;
	return DirectX::SimpleMath::Rectangle(width, height, _size.x, _size.y);
}

void Collider::updateStatusOfCollisions()
{
	if (!_currentCollisions.empty()) _currentCollisions.clear();
	if (!_oldCollisions.empty()) _oldCollisions.clear();
	if (!_finishedCollisions.empty()) _finishedCollisions.clear();
	if (!_tempCollisions.empty()) _tempCollisions.clear();

	for (Collider* collider : GameManager::instance()->getCollidersInSceneWithLayer(_layer)) {
		if (canCollide(collider, this)) {
			_tempCollisions.push_back(collider);
			bool found = false;
			for (Collider* oldCollision : _collisions) {
				if (oldCollision == collider) {
					found = true;
					break;
				}
			}
			if (found) {
				_oldCollisions.push_back(collider);
			}
			else {
				_currentCollisions.push_back(collider);
			}
		}
	}

	for (Collider* prev : _collisions) {
		for (Collider* oldCollider : _oldCollisions) {
			if (prev == oldCollider) {
				_finishedCollisions.push_back(prev);
				break;
			}
		}
	}

	if (!_collisions.empty()) _collisions.clear();

	for (Collider* collision : _tempCollisions) {
		_collisions.push_back(collision);
	}
}

bool Collider::canCollide(Collider* lhs, Collider* rhs)
{
	return !DirectX::SimpleMath::Rectangle::Intersect(lhs->getRectangle(), rhs->getRectangle()).IsEmpty();
}

bool Collider::collide(FLayerMask layermask, ELayer tag)
{
	return layermask == (layermask | tag);
}