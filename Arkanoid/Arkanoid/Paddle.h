#pragma once
#include "GameObject.h"

class Paddle : public GameObject
{
private:
	float _speed;

public:
	Paddle(FTransform nTransform, float nSpeed = 500.f);
	virtual void update(float deltaTime, const Keyboard& keyboard) override;
	virtual void draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;

	// collision check
	virtual void OnCollisionStart(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionStay(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionExit(Collider* lhs, Collider* rhs) override;

	// special methods
	void incrementSize(float multiplier);
	void decrementSize(float multiplier);
	void defaultSize(Vector2 size);
};