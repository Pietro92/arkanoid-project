#include "pch.h"
#include "Powerup.h"
#include "Collider.h"

Powerup::Powerup(FTransform nTransform, DirectX::XMVECTORF32 nPowerupColor) : GameObject(nTransform, DirectX::Colors::Red)
{
	_colliders.push_back(new Collider(this, ELayer::powerup, Vector2::Zero, _transform.size, nPowerupColor));

	_velocity = Vector2::UnitY * 300.f;
}

void Powerup::update(float deltaTime, const Keyboard& keyboard)
{
	GameObject::update(deltaTime, keyboard);
}

void Powerup::draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::draw(batch);
}

void Powerup::OnCollisionStart(Collider* lhs, Collider* rhs)
{
	if (rhs->getLayer() == ELayer::paddle || rhs->getLayer() == ELayer::killZone)
	{
		for (auto col : _colliders)
		{
			if (col != nullptr) delete col;
		}
		_colliders.clear();

		// update color
		_color = DirectX::Colors::Transparent;

		// freeze obj
		_velocity = Vector2::Zero;

		// active powerup if is possible
		if (rhs->getLayer() == ELayer::paddle)	ActivePowerup();
	}
}

void Powerup::OnCollisionStay(Collider* lhs, Collider* rhs) {}

void Powerup::OnCollisionExit(Collider* lhs, Collider* rhs) {}