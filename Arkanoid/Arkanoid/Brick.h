#pragma once
#include "GameObject.h"

class Brick : public GameObject
{
private:
	unsigned short _score;
	unsigned short _hitCount;

public:
	Brick(FTransform nTransform, DirectX::XMVECTORF32 nBrickColor = DirectX::Colors::Blue, unsigned short nScore = 1, unsigned short nHitCount = 1);
	virtual void update(float deltaTime, const Keyboard& keyboard) override;
	virtual void draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;

	// collision check
	virtual void OnCollisionStart(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionStay(Collider* lhs, Collider* rhs) override;
	virtual void OnCollisionExit(Collider* lhs, Collider* rhs) override;

};