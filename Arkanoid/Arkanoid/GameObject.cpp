#include "pch.h"
#include "GameObject.h"

GameObject::GameObject(FTransform nTransform, DirectX::XMVECTORF32 nColor) : _transform(nTransform), _color(nColor), _velocity(Vector2::Zero) 
{ 
	_colliders = std::vector<Collider*>(); 
}

GameObject::~GameObject()
{
	for (Collider* collider : _colliders) {
		if (collider != nullptr) delete collider;
	}
	_colliders.clear();
}

void GameObject::update(float detaTime, const Keyboard& keyboard)
{
	if (_velocity.Length() >= VELOCITY_MIN) _transform.position += _velocity * detaTime;

	// call for any collision update -> collision check [onstart, onstay, onexit]
	for (Collider* col : _colliders)
	{
		col->update();

		for (Collider* startCol : col->getStartColliders())
		{
			OnCollisionStart(col, startCol);
		}
		for (Collider* stayCol : col->getStayColliders())
		{
			OnCollisionStart(col, stayCol);
		}
		for (Collider* exitCol : col->getExitColliders())
		{
			OnCollisionStart(col, exitCol);
		}
	}
}

void GameObject::draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	DirectX::VertexPositionColor vertex1(Vector2(_transform.position.x - _transform.size.x / 2, _transform.position.y - _transform.size.y / 2), _color);
	DirectX::VertexPositionColor vertex2(Vector2(_transform.position.x + _transform.size.x / 2, _transform.position.y - _transform.size.y / 2), _color);
	DirectX::VertexPositionColor vertex3(Vector2(_transform.position.x + _transform.size.x / 2, _transform.position.y + _transform.size.y / 2), _color);
	DirectX::VertexPositionColor vertex4(Vector2(_transform.position.x - _transform.size.x / 2, _transform.position.y + _transform.size.y / 2), _color);
	batch->DrawQuad(vertex1, vertex2, vertex3, vertex4);

	// call for any collision draw
	for (auto col : _colliders)
	{
		col->draw(batch);
	}
}

std::vector<Collider*> GameObject::getCollidersWithLayerMask(FLayerMask layermask) const
{
	std::vector<Collider*> result = std::vector<Collider*>();
	for (auto col : _colliders)
	{
		if(Collider::collide(layermask, col->getLayer())) result.push_back(col);
	}
	return result;
}